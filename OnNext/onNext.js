/**
 * onNext.js
 * =======================================
 * Author: Rhys Devine-Davies (@devine-davies)
 * Created: 2/14/2015
 * Website: https://github.com/Devine-Davies/onNext
 * Description: An advanced light weight plugin for next and previous elements
 */

function OnNextUser()
{   
    /* -- OnNext Events -- */
    this.on_next = function(action, prams)
    {
        /* -- Action safe  -- */
        if(! this.in_list(this.p.events.list , action) )
        {
            console.log('No action found');
            return;
        }

        /* -- Temporarily Record the event -- */
        this.p.events.current = action;
        
        switch(action){
            case 'first' :
                this.run_first_last( this.on_get('first') );
            break;
                
            case 'last' :
                this.run_first_last( this.on_get('last') );
            break;

            case 'next' :
                this.run_next_prev();
            break;

            case 'prev' :
                this.run_next_prev();
            break;

            case 'jump' :
                this.jump( prams );
            break;

            case 'move_to' :
                this.move_to( prams );
            break;
        }
    };
    
    /* -- OnNext Toggles -- */
    this.on_toggle = function(action)
    {
        switch(action)
        {
            case 'cycle' :
                this.opts.cycle = ( this.opts.cycle )? false : true;
            break;
                
            case 'sticky_class' :
                this.opts.sticky_class = ( this.opts.sticky_class )? false : true; 
            break;

            case 'sticky_position' :
                this.opts.sticky_position = ( this.opts.sticky_position )? false : true; 
            break;
        }
    };
    
    /* -- OnNext Get -- */
    this.on_get = function(action, pram)
    {
        /*********************************
        *  Indexing
        -- */
        switch(action)
        {
            case 'index' :
            /* -- Current index -- */
            return this.p.i;

            case 'record_indexs' : 
            /* -- Recorded index array -- */
            return this.p.indexes.r; 
                
            case 'record_index_pos' :
            /* -- Recorded index posistion -- */
            return this.p.indexes.r[ pram ];

            case 'record_index_leng' :
            /* -- Recorded index length -- */
            return this.p.indexes.r.length;
        }
        
        /*********************************
        *  Class infomation
        -- */
        switch(action)
        {   
            case 'class_lag_ammount':
            /* -- Class lag ammount  -- */
            return this.opts.class_lag_am;

            case 'class_next' :
            return this.opts.next_cls_name;

            case 'class_prev' :
            return this.opts.prev_cls_name;
        }
          
        /*********************************
        * Items 
        -- */
        switch(action)
        { 
            case 'first' :
            /* -- First index -- */
            return this.on_get('record_index_pos', 0);
            
            case 'last' :
            /* -- Last index -- */
            return this.on_get('total') -1;
    
            case 'current_item' :
            /* -- Returns the current item being used -- */
            return this.p.items.in_use[ this.on_get('index') ];

            case 'item_index' :
            /* -- Returns the current item being used -- */
            return this.p.items.in_use[ this.on_get('record_index_pos', pram) ];
                
            case 'total' :
            /* -- Total items in current item set -- */
            return this.p.items.in_use.length;
        }
        
        /*********************************
        * Events 
        -- */
        switch(action)
        {  
            /* -- Returns the last recorded event -- */    
            case 'last_recorded_event' :
            return this.p.indexes.r[ this.p.indexes.r -1 ];
                
            /* -- Returns the current recorded event -- */    
            case 'current_event' :
            return this.p.events.current;
        }
                
        /*********************************
        * Other 
        -- */
        switch(action)
        {
            /* -- Returns the current item being used -- */
            case 'current_item_set' :
            return this.opts.isiu;

            case 'sticky_position' :
            return this.opts.sticky_position;
        
            case 'cycle' :
            return this.opts.cycle;
                
            default : 
            console.log('Get not found');
            return null;
        }
    };

    /* -- Get first element -- */    
    this.get_next_elm = function(e , dir)
    {
        e = ( e )? e : this.on_get('current_item');
        var i = 0;
        /* -- Check if is elmment -- */
        while ( (e.nodeType !== 1) ) {
            /* -- to infinity and beyond (ok let's not) -- */
            if(i++ === this.p.items.all.length) { return; }
            e = ((dir === 'next')? e.nextSibling : e.previousSibling);
        }
        
        return e;
    };
    
    /* -- OnNext Update -- */
    this.on_update = function(action, prams)
    {
        switch(action)
        {
            /* -- Update increment -- */
            case 'increment' :
                this.opts.increment = parseInt(prams);
            break;

            /* -- Update increment -- */
            case 'item_set' :
                this.update_item_set( prams );
            break;

            /* -- Update increment -- */
            case 'flush' :
                this.class_flush( );
            break;
                
            default : 
                this.log_alert();
            break;  
        }
    };

	/* -- Update Item Set -- */
	this.update_item_set = function(arg) 
	{	
        var isiu   = this.on_get('current_item_set');
        var in_use = this.p.items.in_use;
		
		switch (arg)
        {
			case 'all':
				isiu   = 'all';
				in_use = this.p.items.all; 	
			break;
                
			case 'excluded':
				isiu   = 'excluded';
				in_use = this.p.items.excluded; 	
			break;
                
			default:
				isiu   = 'active';
				in_use = this.p.items.active;
            break;
		}
        
        this.opts.isiu      = isiu;
        this.p.items.in_use = in_use;
		
		/* -- Reset the index -- */	
		this.set_item_index();
	};
    
    /* -- Clears all item with class applied -- */
    this.class_flush = function()
    {
        var ai = this.p.items.in_use;
        var ci = this.on_get('current_item');
        
        /* -- Loop though all current items -- */
		for (var i = 0; i < ai.length; i++)
		{
            if( (ai[i] !== ci) )
            {
                this.remove_class( ai[i], this.on_get('class_prev') );
                this.remove_class( ai[i], this.on_get('class_next') );
            }
		}
    };

    /* -- Check if item is in array -- */
    this.in_list = function(haystack, needle)
    {
        for (var i = 0; i < haystack.length; i++) 
        {
            if (haystack[i] === needle) 
            {
                return true;
            }
        }
        
        return false;
    };
    
    this.log_alert = function()
    {
        console.log('No action found');
    };
}

function OnNextEvents()
{
    /* -- Before item update -- */
    this.before_update = function( elms )
    {
        var prams = {
            elms       : elms,
            item_set   : this.on_get('current_item_set'),
            type       : 'before_update' 
        };
        this.opts.before_u( prams );   
    };

    /* -- After Item Update -- */
    this.after_update = function( elms )
    {
        var prams = {
            elms       : elms,
            item_set   : this.on_get('current_item_set'),
            type       : 'after_update' 
        };
		this.opts.after_u( prams );   
    };
}

function OnNext(pram)
{
	/* -- Propaties -- */
	this.p = {
        
        indexes : {
        /* -- Index  -- */ c : -1,
        /* -- Record -- */ r : []
        },
        
        /* -- Events -- */ 
        events : {
            list     : ['first', 'last', 'next', 'prev', 'jump', 'move_to' ],
            recorded : [],
            current  : '' 
        },
        
		/* -- Item sets -- */
		items : {
		/* -- All                  -- */ all : [],
		/* -- Active               -- */ active    : [],
		/* -- Excluded, in skipped -- */ excluded  : [],
		/* -- Current, in use      -- */ in_use    : []
		}
    };

	/* -- Options -- */
	this.opts = {
	/* -- obj             -- */ obj             : pram.target             || null,
    /* -- Active position -- */ starting_pos    : pram.starting_position  || 0,
    /* -- Increment       -- */ increment       : pram.increment          || 1,
        
    /* -- Next Class      -- */ next_cls_name   : pram.next_class_name    || 'active',
    /* -- Previous Class  -- */ prev_cls_name   : pram.prev_class_name    || 'prev_active',	
    /* -- Sticky Class    -- */ sticky_class    : pram.sticky_class       || false,
    /* -- Classlag ammout -- */ class_lag_am    : pram.class_lag_ammount  || 2,
	
    /* -- Cycle           -- */ cycle           : pram.cycle	          || false,
    /* -- Skip index      -- */ skip            : pram.skip               || [],	
    /* -- Item set        -- */ isiu            : pram.current_item_set   || 'active',
    /* -- Active position -- */ sticky_position : pram.sticky_position    || false,

	/* -- Before update   -- */ before_u        : pram.before_update      || function(){},
	/* -- After update    -- */ after_u         : pram.after_update       || function(){}
	};
    
    /* -- OnNextUser : user Functionality -- */
    OnNextUser.apply(this);

    /* -- OnNextEvents : user Functionality -- */
    OnNextEvents.apply(this);
    
    /* -- Initialization -- */
	this.init = function()
	{
		/* -- obj -- */
		this.set_objs();
		/* -- This sort -- */
		this.sort_objs();
		/* -- Set the current item set -- */		
		this.on_update( 'item_set', this.on_get('current_item_set') );
	};

	/* -- Set Main Objects -- */
	this.set_objs = function()
	{
		/* -- obj -- */
		var elm  = document.getElementById(this.opts.obj);
        
        if(elm)
        {
            this.opts.obj = elm;
            /* -- Set the list -- */
		    this.p.items.all = this.opts.obj.children;
        }
        else
        {
            console.log('element using id "' + this.opts.obj + '" not found');
        }
	};

	/* -- Strip Excluded Items -- */
	this.sort_objs = function()
	{
		for (var i = 0; i < this.p.items.all.length; i++)
		{
            /* -- Index the item -- */
			this.p.items.all[i].item_index = i;
			
			if( this.opts.skip.indexOf( i ) === -1 )
			{
                /* -- Build active item list -- */
				this.p.items.active.push(this.p.items.all[i]);
			}
			else
			{
                /* -- Build excluded item list -- */
				this.p.items.excluded.push(this.p.items.all[i]);
			}
		}
	};
    
    /* -- Set Item Index  -- */
    this.set_item_index = function()
    {
        (this.opts.starting_pos >= 0)? this.move_to( this.opts.starting_pos ) : null;
    };
    
    /* -- First & last -- */
    this.run_first_last = function( index )
    {
        /* -- Run the update -- */
        this.set_index(index);
        this.run_update( index );
    };
    
    /* -- Run next prev -- */
    this.run_next_prev = function()
    {
        /* -- Condishions -- */
        var con = {
            /* -- Check if update was sucsessful -- */
            one : (this.unary_operators( ((this.on_get('current_event') === 'next')? 'increment' : 'decrement') )),
            /* -- if both true then override update -- */
            two : ( this.on_get('cycle') && this.on_get('sticky_position') )
        };
        
        /* -- check both condishions -- */
        if( (! con.one) || (con.two) ) { this.sticky_position(); }
        
        /* -- Run the update -- */
        this.run_update();
    };

	/* -- Move to -- */
	this.move_to = function( index )
	{
		this.set_index( index );
		
		/* -- If is not false -- */
		if( ! this.check_safe() )
		{
			this.set_index(  this.on_get('last_recorded_event') );
            return false;
		}
        
        this.run_update( index );     
        return true;
	};

	/* -- Jump -- */
	this.jump = function( index )
	{
        this.unary_operators( ((index > 0)? 'increment' : 'decrement'),  index);
        this.run_update();
	};
    
    /* -- Unary operators : increment / decrement -- */
    this.unary_operators = function( unary, amount ) 
    {
		/* -- All index -- */
        var ai = {
        /* -- Current   -- */ c : this.on_get('index'),
        /* -- Recoreded -- */ r : this.on_get('index'),
        };
        
        /* -- If ammount not set then set it -- */
        amount = (amount)? amount : this.opts.increment;
        
        /* -- Ran false -- */
        var rf = false;
        
        /* -- Loop ammount -- */
		for(var i = 0; i < Math.abs( amount ); i++)
		{
            /* -- Get index -- */
            ai.c = this.on_get('index');
            
            /* -- Set the new index, depending on direction -- */
            this.set_index( ((unary === 'increment')? (ai.c += 1) : (ai.c -= 1)) );
            
            /* -- Check to see if index update was sucsessfull -- */
            ( ! this.check_safe() )?  rf = true : null;
		}

        var con = {
        /* -- Check if run flase   -- */ one : rf, 
        /* -- or cycle is not true -- */ two : (! this.on_get('cycle'))
        };
        
        /* -- Check to see we shoud go back to recoreded index -- */
        if( (con.one) && (con.two) ){ this.set_index( ai.r ); }
        
        return rf;
    };
    
    /* -- Record index -- */
    this.record_index = function()
    {
        this.p.indexes.r.push( this.on_get('index') );
    };
    
	/* -- Set Index -- */
	this.set_index = function( i )
	{
		this.p.i = i;
	};
    
    /* -- Record current event -- */
    this.record_event = function(event_name)
    {
        this.p.events.recorded.push( event_name );
    };

	/* -- Increment -- */
	this.increment_deprecated = function()
	{
		var i = this.on_get('index');
		this.set_index( parseInt(i += this.opts.increment) );
	};

	/* -- Decrement -- */
	this.decrement_deprecated = function()
	{
		var i = this.on_get('index');
		this.set_index( i -= this.opts.increment );
	};

	/* -- Check safe -- */
	this.check_safe = function()
	{
		if( this.on_get('index') >= this.on_get('total') )
		{
			/* -- check if cycle is active-- */
			(this.on_get('cycle'))? this.set_index(0) : this.set_index( this.on_get('last') );
			return false;
		}

		else if( this.on_get('index') < 0 )
		{
			(this.on_get('cycle'))? this.set_index( this.on_get('last') ) : this.set_index(0);
			return false;
		}
		
		return true;
	};
    
    /* -- Sticky position -- */
    this.sticky_position = function()
    {
        if( this.on_get('sticky_position') )
        {
            for(var i = 0; i < this.opts.increment; i++)
            {
                var first = this.get_next_elm( this.opts.obj.firstChild , 'next');
                var last  = this.get_next_elm( this.opts.obj.lastChild  , 'prev');

                switch( this.on_get('current_event') )
                {
                    case 'next':
                        this.opts.obj.removeChild(first);
                        this.opts.obj.appendChild(first);
                    break;

                    case 'prev':
                        this.opts.obj.removeChild(last);
                        this.opts.obj.insertBefore(last, first);
                    break;
                }
            }
        }
    };
    
    /* -- Update classes -- */
    this.run_update = function( index )
    {
        /* -- Update the index -- */
        this.set_index( (index)? index : this.on_get('index') );
        
        /* -- Record the current index -- */
        this.record_index();
        
        var cla = this.on_get('class_lag_ammount');
        
        /* -- Items -- */
        var i = {
        /* -- p previous -- */ pp : this.on_get('item_index', (this.on_get('record_index_leng') - (cla + 1)  )),
        /* -- previous   -- */ p  : this.on_get('item_index', (this.on_get('record_index_leng') - cla )),
        /* -- current    -- */ c  : this.on_get('current_item')
        };
        
        /* -- Class update Sucsesfull -- */
        if( i.p !== i.c )
        {
            /* -- Function -- */
            this.before_update( i );
            
            /* -- Record the event -- */
            this.record_event( this.p.events.current );
            
            /* -- Remove classes -- */
            this.remove_class( i.pp, this.on_get('class_prev') );
            this.remove_class( i.p,  this.on_get('class_next') );

            /* -- Add classes -- */
            this.add_class( i.p, this.on_get('class_prev') );
            this.add_class( i.c, this.on_get('class_next') );
            
            /* -- Function -- */
            this.after_update(i);            
        }
        
        /* -- Class update Sucsesfull -- */
        else
        {
            /* -- Remove the last recorded as it was not sucsessful -- */
            this.p.indexes.r.pop();
        }
    };

	/* -- Remove Class -- */
	this.remove_class = function( item, cls )
	{
        /* -- Check for sticky class -- */        
        if( (! this.opts.sticky_class ) )
        {
            if( (item) )
            {
                /* -- Remove class -- */
                item.className = item.className.replace( ' ' + cls, "" );            
            }
        }
	};

	/* -- Update class -- */
	this.add_class = function(item, cls)
	{
        if( item )
        {
            /* -- Look for exsisiting class -- */
            if ( ! this.has_class( item, cls ) )
            {
                /* -- Class update -- */        
                item.className = item.className + ' ' +  cls;
            }
        }
	};
    
    /* -- Has class -- */
    this.has_class = function( item, cls )
    {
        if(item)
        {
            return ( (" " + item.className + " ").indexOf( cls ) > 0);
        }
    };
    
this.init();
//end
}   

