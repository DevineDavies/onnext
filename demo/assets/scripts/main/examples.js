$(function() {
    $('.nav-tabs li a').click(function (e) {
      $(this).tab('show');
      e.preventDefault();
    });
});

$(function() {

    /* -- onNext -- */
    on = new OnNext({
        target             : 'on-next-canvas',
        starting_position  : 0,
        increment          : 1,

        next_class_name    : 'active',
        prev_class_name    : 'prev_active',	
        sticky_class       : false,
        class_lag_ammount  : 2,

        cycle	           : false,
        skip               : [],	
        current_item_set   : 'active',
        sticky_position    : false,

        before_update      : function(pram){},
        after_update       : function(pram){}
    });
    
    // on = new OnNext({ target : 'on-next-canvas', starting_position : 0, increment : 3, cycle : false, sticky_position : false });
    // on = new OnNext({ target : 'on-next-canvas', starting_position : 0, increment : 3, cycle : true,  sticky_position : false });
    // on = new OnNext({ target : 'on-next-canvas', starting_position : 0, increment : 3, cycle : false, sticky_position : true  });
    // on = new OnNext({ target : 'on-next-canvas', starting_position : 0, increment : 3, cycle : true,  sticky_position : true });

    /* -- Events -- */ 
    $('button.action').on('click',  function() { do_event($(this)); });
    $('input.action').on( 'change', function() { do_event($(this)); });
    
    var do_event = function($this) {
        /* -- Events -- */
        ( $this.hasClass('next')  )? on.on_next('next')  : null;
        ( $this.hasClass('prev')  )? on.on_next('prev')  : null;
        ( $this.hasClass('first') )? on.on_next('first') : null;
        ( $this.hasClass('last')  )? on.on_next('last')  : null;
        ( $this.hasClass('jump')  )? on.on_next('jump'   , $('input.jump').val()) : null;
        ( $this.hasClass('move')  )? on.on_next('move_to', $('input.move').val()) : null;

        /* -- Toggles -- */
        ( $this.hasClass('cycle')        )? on.on_toggle('cycle')           : null;
        ( $this.hasClass('sticky-class') )? on.on_toggle('sticky_class')    : null;
        ( $this.hasClass('sticky-pos')   )? on.on_toggle('sticky_position') : null;
        
        /* -- Updates -- */
        ( $this.hasClass('update-increment') )? on.on_update('increment', $this.val() ) : null;
        ( $this.hasClass('flush')            )? on.on_update('flush'    , null        ) : null;
        ( $this.hasClass('item-set')         )? on.on_update('item_set' , $this.val() ) : null;
        
        /* -- Other -- */
        ( $this.hasClass('skip') )? update_icon_set() : null;
    }
});

var update_icon_set = function(skip)
{   
    on.class_flush();
    
    var selected = [];
    $('.multiple-select input:checked').each(function() { selected.push(parseInt($(this).val())); });
    
     on = new OnNext({ 
         target : 'on-next-canvas', 
         skip   : selected
    });
}