<?php
$link = '<script src="onNext.com/onNext.min.js"></script>';

$mu ='
<ul>
  <li>This is a slide.</li>
  <li>This is another slide.</li>
  <li>This is a final slide.</li>
</ul>
';

$wl = "
window.onload = function(){
  var on = new OnNext({target : 'on-next'});
});
";

$o1 = "
/* -- OnNext Obj -- */
var on = new OnNext({
  'target'    : 'on-next',       /* -- -- */
  'skip'      : [],		 /* -- -- */
  'increment' : 1,		 /* -- -- */
  'callback'  : function(){ }    /* -- -- */
});
";

$o2 = " 'target' : 'on-next'";
$o3 = "'skip' : [2, 4, 6]";
$o4 = "'increment' : 3";
$o5 = "'callback'  : function(){ alert('updated'); }";


$e1 = "
on.next();
on.prev();
";

$e2 = "
on.first();
on.last();
";

$e3 = "
on.jump(10);
on.jump(-5);
";

$e4 = "on.move_to(3);";

function show_code($code){
	echo "<pre><code>". htmlentities($code) ."</code></pre>";
}
?>