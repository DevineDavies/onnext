<head>
    <script src="../OnNExt/min/onNext-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
    <link href='http://fonts.googleapis.com/css?family=Muli|Open+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Hind|Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <script src="assets/scripts/min/examples-min.js"></script>
</head>
<body>
    <!-- Billboard -->
    <div class="container-full billboard">
       <div class="row">
            <div class="col-lg-5 text-center v-center">
                <h1>onNext</h1>
                <p class="lead">An advanced light weight plugin for next and previous elements</p>
                <div class="row">
                    <div class="col-xs-6"><a href="https://github.com/Devine-Davies/onNext" target="_blank" >Git hub</a></div>
                    <div class="col-xs-6"><a href="#" target="_blank" >Bower install OnNext</a></div>
                                    
                

                </div>
            </div>
        </div>
    </div>
    <!-- Page container -->   
    <div class="page-container container">
        <?php include_once('partials/ui-exsamples.php'); ?>
    </div>
    <!-- Showcase grid -->  
    <div class="container">
        <section class="row showcase-grid" >
        <ul id="on-next-canvas">
            <?php for($i = 0; $i < 9; $i++){ echo '<li class="col-xs-4" ><p>'.$i.'</p></li>'; } ?>                        
        </ul>
    </section>
    </div>
</body>