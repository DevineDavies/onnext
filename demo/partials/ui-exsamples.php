<section class="app-tools">
    <div role="tabpanel">
        <ul  class="nav nav-tabs" role="tablist">
            <!-- Next & previous -->
            <li role="presentation" class="active">
                <a href="#section-a" aria-controls="section-a" role="tab" data-toggle="tab" data-func='1' >Welcome</a>
            </li>
            <!-- Jumping & moving -->
            <li role="presentation">
                <a href="#section-c" aria-controls="section-b" role="tab" data-toggle="tab" data-func='3' >Jumping & moving</a>
            </li>
            <!-- Skipping -->
            <li role="presentation">
                <a href="#section-d" aria-controls="section-c" role="tab" data-toggle="tab" data-func='4' >Skipping</a>
            </li>
            <!-- More Setting -->
            <li role="presentation">
                <a href="#section-e" aria-controls="section-e" role="tab" data-toggle="tab" data-func='4' >More setting</a>
            </li>
        </ul>
        <div class="tab-content">
            <!-- Next & previous -->
            <div role="tab-pane" id="section-a" class="tab-pane active">
                <h4>Welcome to OnNext</h4>
                <p class="info" >This is just a simple demo to outline all the features of this plugin. </p>
            </div>
            
            <!-- Jumping & moving -->
            <div role="tab-pane" id="section-c" class="tab-pane fade ">
                <div class="form-inline row ">
                    <div class="col-xs-6" >
                        <p class="info">Jumping <span>A Posatvie number looping farward and a negative number backwards.</span></p>
                        <div class="input-group">
                            <div class="input-group-addon">Jump to: </div>
                            <input type="text" class="form-control action jump" id="exampleInputAmount" placeholder="Amount">
                            </div>
                        <button class="btn btn-primary action jump">Jump</button>
                    </div>
                    <div class="col-xs-6" >
                        <p class="info">Moving: <span>Returns true if sucsessfull, false unsuccessful.</span></p>
                        <div class="input-group">
                        <div class="input-group-addon">Move to: </div>
                        <input type="text" class="form-control action move" id="exampleInputAmount" placeholder="Index">
                    </div>
                        <button class="btn btn-primary action move">move</button>
                    </div>
                </div>
            </div>
            
            <!-- Skipping -->
            <div role="tab-pane" id="section-d" class="tab-pane fade ">
                <from class="contraols" >
                    <p class="info" >Select the number you wish to skip.</p>
                    <section class="form-control multiple-select">
                        <?php  for($i = 0; $i < 9; $i++)  { echo ' <label class="checkbox-inline"><input class="skip action" type="checkbox" value="'.$i.'">'.$i.'</label>'; } ?>
                    </section>
                    <br>
                    <div class="row">
                        <div class="col-xs-6 col-md-4 ">
                            <p class="info">Active: <span>Will use all item that are not index.</span></p>
                            <label class="radio-inline"><input type="radio" class="item-set action" name="item-set" value="active" checked>Active</label>
                        </div>
                        <div class="col-xs-6 col-md-4 ">
                            <p class="info">Excluded: <span> Will use all item that are index.</span></p>
                            <label class="radio-inline"><input type="radio" class="item-set action" name="item-set" value="excluded">Excluded </label>
                        </div>
                        <div class="col-xs-6 col-md-4 ">
                            <p class="info">All: <span>Will inclue eveny item.</span></p>
                            <label class="radio-inline"><input type="radio" class="item-set action" name="item-set" value="all">All</label>
                        </div>
                    </div>
                </from>
            </div>
            
            <!-- More Setting -->
            <div role="tab-pane" id="section-e" class="tab-pane fade " >
                <form class="form-inline">
                    <div class=" row">
                        <label class="col-xs-12 col-sm-6 col-md-4">
                            <br/>
                            <p class="info" >Sticky: <span>will hold the class name to the object.</span></p>
                            <input type="checkbox"  class="action sticky-class" value="sticky-class"> Sticky Class
                        </label>
                        <label class="col-xs-12 col-sm-6 col-md-4">
                            <br/>
                            <p class="info"  >Sticky: <span>will hold the class name to the object.</span></p>
                            <input type="checkbox"  class="action sticky-pos" value="sticky-pos"> Sticky position
                        </label>
                        <label class="col-xs-12 col-sm-6 col-md-4">
                            <br/>
                            <p class="info"  >Cycle: <span>if set to ture will look back around.</span></p>
                            <input type="checkbox" class="action cycle" value="toggle-cycle"> Cycle
                        </label>
                        <label class="col-xs-12 col-sm-6 col-md-4">
                            <br/>
                            <p class="info"  >Flush all: <span> will remove all current classes</span></p>
                            <button type="button" class="btn btn-default action flush"> Flush all classes </button>
                        </label>
                    </div>
                </form>
            </div>

            <!-- Make these global -->
            <section class="contraols row " >
                <br>
                <div class="col-md-3">
                    <button type="button" class="btn btn-default action first">First</button>
                    <button type="button" class="btn btn-default prev action">Previous</button>
                    <button type="button" class="btn btn-default next action">Next</button>
                    <button type="button" class="btn btn-default action last">Last</button>
                </div>
                <div class="col-md-4">
                    <label class="input-group">
                    <div class="input-group-addon">Increment</div>
                    <input type="text" class="form-control action update-increment" id="exampleInputAmount" placeholder="Amount" style="width:150px; float:none">
                </label>
                </div>
            </section>
        </div>
    </div>
</section>
