#OnNext
An advanced light weight plugin for next and previous elements

##bower
*Bower install OnNext*

##Object declaration
*All parameters given are default*
```javascript
var on = new OnNext({
    target             : on-next,
    starting_position  : 0,
    increment          : 1,

    next_class_name    : 'active',
    prev_class_name    : 'prev_active',	
    sticky_class       : false,
    class_lag_ammount  : 2,

    cycle	           : false,
    skip               : [],	
    current_item_set   : 'active',
    sticky_position    : false,

    before_update      : function(pram){},
    after_update       : function(pram){}
});
```
##Parameters
Parameter         | Type    | Action
----------------- | ------- | -------------
target            | string  | *id of targeted element*
starting_position | int     | *(No start: -1), Any other index given will start at that point*
increment         | int     | *Threshold to jump by*
next_cls_name     | string  | *class name to add to next elm*
prev_cls_name     | string  | *class name to add to previous elm*
sticky_class      | bool    | *if ture will stop removing class*
class_lag_ammount | int     | *Ammount of items to keep active at once*
cycle             | bool    | *Will allow jump from last to first, or first to last*
skip              | array   | *Will skip out indexes found in posistion* 
current_item_set  | string  | *(defult: 'active'), ('excluded'), ('all')*
before_update     | fun     | *Before class is added*
after_update      | fun     | *After class has been added*

##on_next
```javascript
on.on_next('next');
on.on_next('prev');
on.on_next('first'); 
on.on_next('last');
on.on_next('jump', index);
on.on_next('move_to', index);
```
Parameter  | Desc
---------- | -------------
next       | *Move to the next element.*
prev       | *Move to the previous element.*
first      | *Jump to first.*
last       | *Jump to last.*
jump       | *Positive number looping farward, negative loop back.*
move_to    | *Move to a given index if found.*

##on_toggle
```javascript
on.on_toggle('cycle');
on.on_toggle('sticky_class');
on.on_toggle('sticky_position');
```
Parameter       | Desc
--------------- | -------------
cycle           | *Allow you to loop forwards and backwards around items.*
sticky_class    | *Keep the active class present.*
sticky_position | *Active item will not move from current position.*

##on_update
```javascript
on.on_update('increment', amount );
on.on_update('flush' );
on.on_update('item_set' , new_item_set )
```
Update type  | Pram type  | Pram
----------   |------------| -------------
increment    | Int        | *Determines jump amount*
flush        | null       | *Remove all classes*
item_set     | String     | *(active = [1,3,5,7,9]), (excluded = [2,4,6,8,10]), (all = [1,2,3,4,5,6,7,8,9,10])*

##Events
```javascript
before_update : function(pram){ console.log(pram) },
after_update : function(pram){ console.log(pram) },
```
